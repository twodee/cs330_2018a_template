#!/usr/bin/env zsh

# ---------------------------------------------------------------------------- 

# Assert that each file has the required tokens.
function check_tokens() {
  file=$1
  shift
  for i in $*; do
    grep -- $i $file >/dev/null
    if [[ $? -ne 0 ]]; then
      echo "Yikes! Expected $i in $file not found." >& 2
      exit 1
    fi
  done
}

# ---------------------------------------------------------------------------- 

iskey=0
isgrader=0
onlylater=0
while getopts kgl opt; do
  case $opt in
    (k)
      iskey=1
      ;;
    (g)
      isgrader=1
      ;;
    (l)
      onlylater=1
      ;;
  esac
done

graderdir=${0:A:h}
srcdir=$(pwd)
tmpdir=$srcdir/../.tmp
basedir=${srcdir:t}
name="wasd"
course=cs330
semester=2018a
actual_version=1

# Assert version
read expected_version < =(curl -s "https://twodee.org/teaching/vspec.php?course=$course&semester=$semester&homework=$name")
if [[ $expected_version != $actual_version ]]; then
  echo "Your SpecChecker appears to be out of date. Follow the directions "
  echo "in homework 0, part 3 to synchronize it."
  echo
  exit 1
fi

# Assert that we're in the right directory.
if [[ $basedir != $name ]]; then
  echo "Oh no! The grader must be run from the $name directory." >&2
  exit 1
fi

# ---------------------------------------------------------------------------- 

# Assert that all required files exist.
setopt RC_EXPAND_PARAM
expected_files=(Tuple2.java Vector2.java World.java Player.java BooleanTask.java ThreadPool.java Raycaster.java Main.java)
expected_files=(src/wasd/${^expected_files})

for i in $expected_files; do
  if [[ ! -f $i ]]; then
    echo "Whoops! Expected file $i not found." >&2
    exit 1
  fi
done

# ---------------------------------------------------------------------------- 

mkdir -p $tmpdir

if [[ ! -f $tmpdir/junit.jar ]]; then
  echo "Downloading JUnit..."
  (cd $tmpdir && curl -o junit.jar 'http://search.maven.org/remotecontent?filepath=junit/junit/4.12/junit-4.12.jar')
fi

if [[ ! -f $tmpdir/hamcrest.jar ]]; then
  echo "Downloading Hamcrest..."
  (cd $tmpdir && curl -o hamcrest.jar 'http://search.maven.org/remotecontent?filepath=org/hamcrest/hamcrest-core/1.3/hamcrest-core-1.3.jar')
fi

BUILDPATH=$tmpdir/junit.jar:$tmpdir/hamcrest.jar:.:$graderdir/specchecker_wasd.jar:$HOME/checkouts/speccheck/bin

# Delete any old class files.
echo "Removing any old class files..."
rm -rf $tmpdir/*.class

# Compile their stuff.
echo "Compile Java source files with \"javac -cp $BUILDPATH:$tmpdir -d $tmpdir $expected_files\"..."
javac -cp $BUILDPATH:$tmpdir -d $tmpdir $expected_files
if [[ $? -ne 0 ]]; then
  exit 1
fi
echo "Success."
echo

# Try SpecChecker.
echo "Running SpecChecker with \"java -cp $BUILDPATH:$tmpdir wasd.speccheck.SpecCheckerGatekeeper\"..."
java -cp $BUILDPATH:$tmpdir wasd.speccheck.SpecCheckerGatekeeper
if [[ $? -ne 0 ]]; then
  exit 1
fi
echo
echo "90% success."
echo

echo "Now we'll manually test a threadpool of size 1 with \"java -cp $tmpdir wasd.Main $graderdir/maps/world3 1\"..."
echo "Please walk around, show the overlay, and assert that the program behaves as expected."
echo "Hit <Escape> when you have done so."
java -cp $tmpdir wasd.Main $graderdir/maps/world3 1

echo
echo -n "Did that work? y or [n]? "
read answer
echo
if [[ "$answer" != "y" ]]; then
  echo "The test of world3 didn't behave as specified."
  exit 1
fi

echo "Now we'll manually test a threadpool of size 4 with \"java -cp $tmpdir wasd.Main $graderdir/maps/world2 4\"..."
echo "Please walk around, show the overlay, and assert that the program behaves as expected."
echo "Hit <Escape> when you have done so."
java -cp $tmpdir wasd.Main $graderdir/maps/world2 4

echo
echo -n "Did that work? y or [n]? "
read answer
echo
if [[ "$answer" != "y" ]]; then
  echo "The test of world2 didn't behave as specified."
  exit 1
fi

# ----------------------------------------------------------------------------

# Assert that a commit and push has happened or is about to happen.
if [[ $isgrader -eq 0 ]]; then
  echo -n "Did you use lambdas in ThreadPool and Raycaster? y or [n]? "
  read answer
  echo
  if [[ "$answer" != "y" ]]; then
    exit 1
  fi

  echo -n "Are you using the threadpool in Raycaster? y or [n]? "
  read answer
  echo
  if [[ "$answer" != "y" ]]; then
    exit 1
  fi

  echo "Have you added any unadded files, committed, and pushed to Bitbucket?"
  echo -n "Run \"git status\" if you don't know. y or [n]? "
  read answer
  echo
  if [[ "$answer" != "y" ]]; then
    exit 1
  fi
fi

exit 0
