import BullsAndCows(nonbulls,bulls,cows,score)
import System.Exit

-- --------------------------------------------------------------------------- 

status = 2

assertGeneralEquals message isEqual expected actual = do
  if isEqual expected actual then
    return ()
  else do
    putStrLn message
    putStr $ "  Expected: "
    print expected
    putStr $ "    Actual: "
    print actual
    System.Exit.exitWith (ExitFailure status)
    return ()

assertEquals message expected actual = 
  assertGeneralEquals message (==) expected actual

assertTrue message actual = assertEquals message True actual
assertFalse message actual = assertEquals message False actual

-- --------------------------------------------------------------------------- 

assertNonbulls target guess expected =
  assertEquals ("nonbulls " ++ (show target) ++ " " ++ (show guess) ++ " gives an unexpected value.") expected $ nonbulls target guess

assertBulls target guess expected =
  assertEquals ("bulls " ++ (show target) ++ " " ++ (show guess) ++ " gives an unexpected value.") expected $ bulls target guess

assertCows target guess expected =
  assertEquals ("cows " ++ (show target) ++ " " ++ (show guess) ++ " gives an unexpected value.") expected $ cows target guess

assertScore target guess expected =
  assertEquals ("score " ++ (show target) ++ " " ++ (show guess) ++ " gives an unexpected value.") expected $ score target guess

assertBullsAndCows target guess expectedNonbulls expectedBulls expectedCows expectedScore = do
  assertNonbulls target guess expectedNonbulls
  assertBulls target guess expectedBulls
  assertCows target guess expectedCows
  assertScore target guess expectedScore

-- --------------------------------------------------------------------------- 

main = do
  assertBullsAndCows "930" "593" ("930","593") "" "93" (0, 2)
  assertBullsAndCows "452" "850" ("42","80") "5" "" (1, 0)
  assertBullsAndCows "190" "748" ("190","748") "" "" (0, 0)
  assertBullsAndCows "274" "746" ("274","746") "" "74" (0, 2)
  assertBullsAndCows "123" "123" ("", "") "123" "" (3, 0)
  assertBullsAndCows "1239" "1238" ("9", "8") "123" "" (3, 0)
  assertBullsAndCows "12398" "12389" ("98", "89") "123" "89" (3, 2)
  assertBullsAndCows "186932" "590316" ("186932","590316") "" "9316" (0, 4)
  assertBullsAndCows "17890" "16259" ("7890","6259") "1" "9" (1, 1)
